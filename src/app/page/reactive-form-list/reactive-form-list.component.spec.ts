import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveFormListComponent } from './reactive-form-list.component';

describe('ReactiveFormListComponent', () => {
  let component: ReactiveFormListComponent;
  let fixture: ComponentFixture<ReactiveFormListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveFormListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveFormListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

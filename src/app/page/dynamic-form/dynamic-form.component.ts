// DynamicFormComponent is the entry point and the main container for the form.
// It presents a list of questions, each bound to a <df-question> component element. 
// The <df-question> tag matches the DynamicFormQuestionComponent, the component 
// responsible for rendering the details of each individual question based on values 
// in the data-bound question object.

// DynamicFormComponent expects the list of questions in the form of an array bound 
// to @Input() questions.

import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionBase } from '../../model/question-base';
import { QuestionControlService } from '../../service/question-control.service';

@Component({
  selector: 'dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css'],
  providers: [ QuestionControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() questions: QuestionBase<any>[] = [];
  form: FormGroup;
  payLoad = '';
 
  constructor(private qcs: QuestionControlService) {  }
 
  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }
 
  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
  }
}

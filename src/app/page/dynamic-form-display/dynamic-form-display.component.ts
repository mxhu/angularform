// Finally, display an instance of the form in the AppComponent shell.

import { Component } from '@angular/core';

import { QuestionService } from '../../service/question.service';

@Component({
  selector: 'app-dynamic-form-display',
  templateUrl: './dynamic-form-display.component.html',
  styleUrls: ['./dynamic-form-display.component.css'],
  providers: [ QuestionService ]
})
export class DynamicFormDisplayComponent {

  questions: any[];
  
  constructor(service: QuestionService) {
    this.questions = service.getQuestions();
  }
}

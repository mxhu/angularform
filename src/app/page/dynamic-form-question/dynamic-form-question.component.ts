// Notice this component can present any type of question in your model. 
// You only have two types of questions at this point but you can imagine 
// many more. The ngSwitch (in .html) determines which type of question to display.

// In both dynamic form components you're relying on Angular's formGroup to connect the  
// template HTML to the underlying control objects, populated from the question model with 
// display and validation rules.

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionBase } from '../../model/question-base';

@Component({
  selector: 'df-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['./dynamic-form-question.component.css']
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid; }
}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { User } from '../../model/user';
import { UserService } from '../../service/user.service';
import { emailValidator } from '../../directive/valid-email.directive';

@Component({
  selector: 'app-register-reactive',
  templateUrl: './register-reactive.component.html',
  styleUrls: ['./register-reactive.component.css']
})
export class RegisterReactiveComponent implements OnInit {
  submitted = false;
  titles = ['Mr.', 'Miss.', 'Mrs.'];
  countries = ['Australia', 'China', 'USA'];
  registerForm: FormGroup;
  user: User;

  constructor(private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      // The first item is the initial value for name; the second is the required validator, Validators.required.
      //Reactive validators are simple, composable functions. Configuring validation is harder in template-driven forms where you must wrap validators in a directive.
      title: ['Mr.'],
      familyName: [''],
      firstName: [''],
      livingCountry: ['Australia'],
      phoneNum: ['', emailValidator(/^\+?[0-9]+$/)],
      eMail: ['', emailValidator(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/)]
    })
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    this.user = this.registerForm.value;
    this.user.userID = UserService.getUserID();
    console.log(JSON.stringify(this.user));
  }
}

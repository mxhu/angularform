import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

export function emailValidator(emailRe: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any; } => {
    if (control.value == '') return null;
    const valid = emailRe.test(control.value);
    return  valid? null : {'invalidEmail': {value: control.value}};
  }
}

@Directive({
  selector: '[appValidEmail]',
  providers: [{provide: NG_VALIDATORS, useExisting: ValidEmailDirective, multi: true}]
})
export class ValidEmailDirective implements Validator{
  @Input() appValidEmail: string;

  validate(control: AbstractControl): { [key: string]: any; } {
    return this.appValidEmail ? emailValidator(new RegExp(this.appValidEmail, 'i'))(control) : null;
  }

  constructor() { }

}

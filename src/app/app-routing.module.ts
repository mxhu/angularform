import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './page/register/register.component'
import { RegisterReactiveComponent } from './page/register-reactive/register-reactive.component';
import { DynamicFormDisplayComponent } from './page/dynamic-form-display/dynamic-form-display.component';
import { ReactiveFormDisplayComponent } from './page/reactive-form-display/reactive-form-display.component';

const routes: Routes = [
  { path: '', redirectTo: '/registe', pathMatch: 'full'},
  { path: 'registe', component: RegisterComponent },
  { path: 'reactiveregiste', component: RegisterReactiveComponent},
  { path: 'dynamicform', component: DynamicFormDisplayComponent},
  { path: 'reactiveform', component: ReactiveFormDisplayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

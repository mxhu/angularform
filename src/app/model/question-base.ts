// The next step is to define an object model that can describe all
// scenarios needed by the form functionality. The hero application
// process involves a form with a lot of questions. The question is 
// the most fundamental object in the model.

// From this base you can derive two new classes in TextboxQuestion 
// and DropdownQuestion that represent textbox and dropdown questions. 
// The idea is that the form will be bound to specific question types 
// and render the appropriate controls dynamically.
export class QuestionBase<T>{
  value: T;
  key: string;
  label: string;
  required: boolean;
  order: number;
  controlType: string;

  constructor(options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      order?: number,
      controlType?: string
    } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.required = !!options.required;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
  }
}
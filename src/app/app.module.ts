import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './page/register/register.component';
import { ValidEmailDirective } from './directive/valid-email.directive';
import { RegisterReactiveComponent } from './page/register-reactive/register-reactive.component';
import { DynamicFormComponent } from './page/dynamic-form/dynamic-form.component';
import { DynamicFormQuestionComponent } from './page/dynamic-form-question/dynamic-form-question.component';
import { DynamicFormDisplayComponent } from './page/dynamic-form-display/dynamic-form-display.component';
import { ReactiveFormComponent } from './page/reactive-form/reactive-form.component';
import { ReactiveFormDisplayComponent } from './page/reactive-form-display/reactive-form-display.component';
import { ReactiveFormListComponent } from './page/reactive-form-list/reactive-form-list.component';

import { UserService } from './service/user.service';
import { HeroService } from './service/hero.service';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    ValidEmailDirective,
    RegisterReactiveComponent,
    DynamicFormComponent,
    DynamicFormQuestionComponent,
    DynamicFormDisplayComponent,
    ReactiveFormComponent,
    ReactiveFormDisplayComponent,
    ReactiveFormListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ 
    UserService,
    HeroService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
